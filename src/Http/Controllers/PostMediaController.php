<?php

namespace LoopCraft\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use LoopCraft\Blog\Models\Post;

class PostMediaController extends Controller
{
  protected function attach($postId, $categoryId)
  {
      return Post::findOrFail($postId)->media()->syncWithoutDetaching([$categoryId]);
  }

  protected function detach($postId, $categoryId)
  {
      return Post::findOrFail($postId)->media()->detach($categoryId);
  }

}

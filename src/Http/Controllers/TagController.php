<?php

namespace LoopCraft\Blog\Http\Controllers;

use LoopCraft\Blog\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\View\View;
use LoopCraft\Blog\Traits\CrudDeleteTrait;
use LoopCraft\Blog\Traits\CrudIndexTrait;
use LoopCraft\Blog\Traits\CrudStoreTrait;
use LoopCraft\Blog\Traits\CrudUpdateTrait;
use LoopCraft\Blog\Traits\MakeValidatorTrait;

class TagController extends Controller
{
  use CrudIndexTrait;
  use CrudStoreTrait;
  use CrudUpdateTrait;
  use CrudDeleteTrait;
  use MakeValidatorTrait;

  public function getModel($id = null)
  {
    return $id ? Tag::findOrFail($id) : app()->make(Tag::class);
  }

}

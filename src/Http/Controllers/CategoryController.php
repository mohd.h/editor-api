<?php

namespace LoopCraft\Blog\Http\Controllers;

use Illuminate\Http\Request;
use LoopCraft\Blog\Models\Category;
use LoopCraft\Blog\Traits\CrudDeleteTrait;
use LoopCraft\Blog\Traits\CrudIndexTrait;
use LoopCraft\Blog\Traits\CrudStoreTrait;
use LoopCraft\Blog\Traits\CrudUpdateTrait;
use LoopCraft\Blog\Traits\MakeValidatorTrait;

class CategoryController extends Controller
{
  use CrudIndexTrait;
  use CrudStoreTrait;
  use CrudUpdateTrait;
  use CrudDeleteTrait;
  use MakeValidatorTrait;

  public function getModel($id = null)
  {
    return $id ? Category::findOrFail($id) : app()->make(Category::class);
  }
}


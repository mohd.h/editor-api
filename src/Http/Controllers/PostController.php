<?php

namespace LoopCraft\Blog\Http\Controllers;

use LoopCraft\Blog\Models\Post;
use Illuminate\Http\Request;
use Illuminate\View\View;
use LoopCraft\Blog\Traits\CrudDeleteTrait;
use LoopCraft\Blog\Traits\CrudIndexTrait;
use LoopCraft\Blog\Traits\CrudShowTrait;
use LoopCraft\Blog\Traits\CrudStoreTrait;
use LoopCraft\Blog\Traits\CrudUpdateTrait;
use LoopCraft\Blog\Traits\MakeValidatorTrait;

class PostController extends Controller
{
  use CrudIndexTrait;
  use CrudShowTrait;
  use CrudStoreTrait;
  use CrudUpdateTrait;
  use CrudDeleteTrait;
  use MakeValidatorTrait;

  public function getModel($id = null)
  {
    return $id ? Post::findOrFail($id) : app()->make(Post::class);
  }
    
}

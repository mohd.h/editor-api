<?php

namespace LoopCraft\Blog\Http\Controllers;

use LoopCraft\Blog\Models\Post;
use Illuminate\Http\Request;
use Illuminate\View\View;


class PostCategoryController extends Controller
{

    protected function attach($postId, $categoryId)
    {
        return Post::findOrFail($postId)->categories()->syncWithoutDetaching([$categoryId]);
    }

    protected function detach($postId, $categoryId)
    {
        return Post::findOrFail($postId)->categories()->detach($categoryId);
    }
}

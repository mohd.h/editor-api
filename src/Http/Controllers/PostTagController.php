<?php

namespace LoopCraft\Blog\Http\Controllers;

use LoopCraft\Blog\PostTag;
use Illuminate\Http\Request;
use Illuminate\View\View;
use LoopCraft\Blog\Models\Post;

class PostTagController extends Controller
{
  protected function attach($postId, $categoryId)
  {
      return Post::findOrFail($postId)->tags()->syncWithoutDetaching([$categoryId]);
  }

  protected function detach($postId, $categoryId)
  {
      return Post::findOrFail($postId)->tags()->detach($categoryId);
  }

    
}

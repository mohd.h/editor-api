<?php

namespace LoopCraft\Blog\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class Category extends BaseModel
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug',
        'label',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
    ];

    public function validationRules($data = []) {
        return [
            'slug' => ['required', 'alpha_dash', Rule::unique('posts')->ignore($data['id'] ?? null, 'id')],
            'label' => ['required', 'string'],
        ];
     }
}

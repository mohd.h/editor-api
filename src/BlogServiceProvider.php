<?php

namespace LoopCraft\Blog;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use LoopCraft\Blog\Commands\BlogCommand;

class BlogServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('blog')
            ->hasConfigFile()
            ->hasRoute('api')
            // ->hasViews()
            ->hasMigrations([
                '2023_03_02_050527_create_posts_table',
                '2023_03_02_050528_create_categories_table',
                '2023_03_02_050529_create_tags_table',
                '2023_03_02_050530_create_media_table',
                '2023_03_02_050531_create_category_post_table',
                '2023_03_02_050532_create_post_tag_table',
                '2023_03_02_050533_create_media_post_table',
            ])
            ->runsMigrations()
            ->hasCommand(BlogCommand::class);
    }









    
}

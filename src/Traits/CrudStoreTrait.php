<?php

namespace LoopCraft\Blog\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait CrudStoreTrait {

    // eg: method: POST url: /api/posts

    public function store(Request $request) {
      $data = $request->all();
      $model = $this->getModel();
      $validator = $this->makeValidator($data, $model);
      if($validator->fails()) {
        return response()->json([
          'errors' => $validator->errors()->all()
        ], 400);
      }
      $createdModel = $model->create(
        array_merge(
          $validator->validated(), 
          ['created_by' => Auth::id()]
        )
      );
      return response()->json($createdModel, 201);
    }
}
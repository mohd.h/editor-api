<?php

namespace LoopCraft\Blog\Traits;

use Illuminate\Http\Request;

trait CrudIndexTrait {

    // eg: method: GET url: /api/posts
    public function index(Request $request) {
      $data = $this->getModel()->paginate();
      return response()->json($data, 200);
    }
}
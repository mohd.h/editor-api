<?php

namespace LoopCraft\Blog\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

trait MakeValidatorTrait {

    // eg: method: POST url: /api/posts

    public function makeValidator($data, $model, $ignoreRequired = false) {
      $rules = $ignoreRequired ?
        collect($model->validationRules($data))->map(function ($rule) { 
          Arr::forget($rule, 'required'); 
          return $rule;
        })->all() : $model->validationRules();
      return Validator::make($data, $rules);
    }

    public function getRules($model, $ignoreRequired = false)
    {
      
    }
}
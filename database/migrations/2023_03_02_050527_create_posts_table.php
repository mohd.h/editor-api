<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title', 200);
            $table->string('slug', 200);
            $table->string('status', 50);
            $table->date('published_on')->nullable();
            $table->foreignId('published_by')->nullable()->constrained('users');
            $table->foreignId('cover_image_id')->constrained('media');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->boolean('featured')->default(false);
            $table->text('content');
            $table->string('link', 200)->nullable();
            $table->string('tint', 20)->nullable();
            $table->json('soe')->nullable();
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('posts');
    }
};

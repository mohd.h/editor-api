<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix' => 'api'], function ()
{
    Route::apiResource('posts', LoopCraft\Blog\Http\Controllers\PostController::class);

    Route::apiResource('categories', LoopCraft\Blog\Http\Controllers\CategoryController::class);

    Route::apiResource('tags', LoopCraft\Blog\Http\Controllers\TagController::class);

    Route::apiResource('media', LoopCraft\Blog\Http\Controllers\MediaController::class);

    Route::post('posts/{postId}/categories/{categoryId}', [LoopCraft\Blog\Http\Controllers\PostCategoryController::class, 'attach']);
    Route::delete('posts/{postId}/categories/{categoryId}', [LoopCraft\Blog\Http\Controllers\PostCategoryController::class, 'detach']);

    
    Route::post('posts/{postId}/tags/{tagId}', [LoopCraft\Blog\Http\Controllers\PostTagController::class, 'attach']);
    Route::delete('posts/{postId}/tags/{tagId}', [LoopCraft\Blog\Http\Controllers\PostTagController::class, 'detach']);

    
    Route::post('posts/{postId}/media/{mediaId}', [LoopCraft\Blog\Http\Controllers\PostMediaController::class, 'attach']);
    Route::delete('posts/{postId}/media/{mediaId}', [LoopCraft\Blog\Http\Controllers\PostMediaController::class, 'detach']);


});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
